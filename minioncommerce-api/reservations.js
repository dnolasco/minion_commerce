import uuid from "uuid";
import * as dynamoDbLib from "./libs/dynamodb-lib";
import * as mailerHandler from "./mailer_handler";
import { success, failure } from "./libs/response-lib";


export async function create(event, context) {
  // Request body is passed in as a JSON encoded string in 'event.body'
  const data = JSON.parse(event.body);

  const params = {
    TableName: "reservations",
    Item: {
      userId: event.requestContext.identity.cognitoIdentityId,
      reservationId: uuid.v1(),
      productId: data.productId,
      fullname: data.fullname,
      email: data.email,
      cpf: data.cpf,
      telephone: data.telephone,
      address: data.address,
      cep: data.cep,
      city: data.city,
      state: data.state,
      createdAt: Date.now()
    }
  };

  try {
    await dynamoDbLib.call("put", params);
    await mailerHandler.send(params);
    return success(params.Item);
  } catch (e) {
    console.log(e);
    return failure({ status: false });
  }
}