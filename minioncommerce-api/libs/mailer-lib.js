import https from "https"
import http from "http"
import AWS from "aws-sdk";

const ses = new AWS.SES();

export function send(params) {

  return ses.sendEmail(params).promise(); 

}