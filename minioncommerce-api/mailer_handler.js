import * as mailerLib from "./libs/mailer-lib";
const config = require('./mailer_config.json');

export function send(params) {

  const item = params.Item;
  
  const emailParams = {
    Destination: {
      ToAddresses: [config.to],
    },
    Message: {
      Subject: {
        Data: "Confirmacao de Reserva",
        Charset: "UTF-8"
      },
      Body: {
        Text: {
          Data: "Confirmacao de reserva do cliente: " + item.userId + ". Dados da reserva:\n\n\nReserva \: " + item.reservationId + 
          "\nProduto: " + item.productId + 
          "\nNome completo: " + item.fullname + 
          "\nEmail: " + item.email + 
          "\nCPF: " + item.cpf + 
          "\nTelefone: " + item.telephone + 
          "\nEndereco: " + item.address + 
          "\nCEP: " + item.cep + 
          "\nCidade: " + item.city + 
          "\nEstado: " + item.state,
          Charset: "UTF-8"
        }
      }
    },
    Source: config.from
  };

  return mailerLib.send(emailParams)

}