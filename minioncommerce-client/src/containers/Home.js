import React, { Component } from "react";
import { Button, Carousel } from "react-bootstrap";
import "./Home.css";

export default class Home extends Component {

  render() {
    const minionInfo = {
      bob: {
        text: "Bob gosta de histórias antes de dormir e de brincar com o seu ursinho, Tim.",
        urlimg: "bob.jpg"
      },
      kevin: {
        text: "Kevin adora fazer graça das pessoas ou Minions. Em Minions, ele liderou o trio na procura por um novo Mestre.",
        urlimg: "kevin.jpg"
      },
      stuart: {
        text: "Stuart é brincalhão e engraçado. Ele é bom em videogames e prefere descansar do que obedecer as regras.",
        urlimg: "stuart.jpg"
      }
    }
    return (
            <div className="row">
            <h2 class="home-title">Reserve aqui e agora seus minions em miniatura e crie seu próprio exército minion!</h2>

              <div className="col-md-12">

                <div className="row carousel-holder">

                    <div className="col-md-12">
                      <Carousel>
                        <Carousel.Item>
                          <img width={400} height={450} alt="900x500" src={minionInfo.bob.urlimg} />
                          <Carousel.Caption>
                            <h3>Bob miniatura</h3>
                            <p>{minionInfo.bob.text}</p>
                          </Carousel.Caption>
                        </Carousel.Item>
                        <Carousel.Item>
                          <img width={400} height={450} alt="900x500" src={minionInfo.kevin.urlimg} />
                          <Carousel.Caption>
                            <h3>Kevin Miniatura</h3>
                            <p>{minionInfo.kevin.text}</p>
                          </Carousel.Caption>
                        </Carousel.Item>
                        <Carousel.Item>
                          <img width={400} height={450} alt="900x500" src={minionInfo.stuart.urlimg} />
                          <Carousel.Caption>
                            <h3>Stuart Miniatura</h3>
                            <p>{minionInfo.stuart.text}</p>
                          </Carousel.Caption>
                        </Carousel.Item>
                      </Carousel>
                    </div>

                </div>

                <div className="row">

                    <div className="col-sm-4 col-lg-4 col-md-4">
                        <div className="thumbnail">
                            <img width={350} height={350} src={minionInfo.bob.urlimg} alt=""/>
                            <div className="caption">
                                <h4><a href="reservations/new">Bob</a>
                                </h4>
                                <p>{minionInfo.bob.text}</p>
                            </div>
                            <Button href="reservations/new" bsStyle="warning" bsSize="large" block>Reserve Já</Button>
                        </div>
                    </div>

                    <div className="col-sm-4 col-lg-4 col-md-4">
                        <div className="thumbnail">
                            <img width={350} height={350} src={minionInfo.kevin.urlimg} alt=""/>
                            <div className="caption">
                                <h4><a href="reservations/new">Kevin</a>
                                </h4>
                                <p>{minionInfo.kevin.text}</p>
                            </div>
                            <Button href="reservations/new" bsStyle="warning" bsSize="large" block>Reserve Já</Button>
                        </div>
                    </div>

                    <div className="col-sm-4 col-lg-4 col-md-4">
                        <div className="thumbnail">
                            <img width={350} height={350} src={minionInfo.stuart.urlimg} alt=""/>
                            <div className="caption">
                                <h4><a href="reservations/new">Stuart</a>
                                </h4>
                                <p>{minionInfo.stuart.text}</p>
                            </div>
                            <Button href="reservations/new" bsStyle="warning" bsSize="large" block>Reserve Já</Button>
                        </div>
                    </div>

                </div>

              </div>
            </div>

    );
  }
}