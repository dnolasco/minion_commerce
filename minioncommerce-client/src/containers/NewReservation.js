import React, { Component } from "react";
import { Well, FormGroup, FormControl, ControlLabel, Radio } from "react-bootstrap";
import LoaderButton from "../components/LoaderButton";
import { API } from "aws-amplify";
import "./NewReservation.css";

export default class NewReservation extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: null,
      productId: "",
      fullname: "",
      email: "",
      cpf: "",
      telephone: "",
      address: "",
      cep: "",
      city: "",
      state: ""
    };
  }

  validateForm() {
    return this.state.fullname.length > 0 &&
    this.state.productId.length > 0 &&
    this.state.email.length > 0 &&
    this.state.cpf.length > 0 &&
    this.state.telephone.length > 0 &&
    this.state.address.length > 0 &&
    this.state.cep.length > 0 &&
    this.state.city.length > 0 &&
    this.state.state.length > 0;
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });

  }

  handleOptionChange = event => {
    this.setState({
      productId: event.target.value
    });
  }

  handleSubmit = async event => {
    event.preventDefault();

    this.setState({ isLoading: true });

    try {
      await this.createReservation({
        productId: this.state.productId,
        fullname: this.state.fullname,
        email: this.state.email,
        cpf: this.state.cpf,
        telephone: this.state.telephone,
        address: this.state.address,
        cep: this.state.cep,
        city: this.state.city,
        state: this.state.state
      });
      this.props.history.push("/");
    } catch (e) {
      alert(e);
      this.setState({ isLoading: false });
    }
  }

  createReservation(reservation) {
    return API.post("reservations", "/reservations", {
      body: reservation
    });
  }

  render() {
    return (
      <div className="NewReservation">
        <h3>Faça sua Reserva:</h3>
        <Well>Passo 1: Escolha a sua miniatura</Well>
        <form onSubmit={this.handleSubmit}>
          <FormGroup controlId="productId">
            <Radio 
              name="radioGroup"
              onChange={this.handleOptionChange}
              value="1"
            inline>
              <p>Bob</p>
            </Radio>{' '}
              <img width={300} height={300} src="https://c.shld.net/rpx/i/s/i/spin/10065584/prod_1489158912??hei=64&wid=64&qlt=50" alt=""/>
            <Radio 
              name="radioGroup"
              onChange={this.handleOptionChange}
              value="2"
            inline>
              <p>Kevin</p>
            </Radio>{' '}
              <img width={300} height={300} src="https://c.shld.net/rpx/i/s/i/spin/10065584/prod_1489159012??hei=64&wid=64&qlt=50" alt=""/>
            <Radio 
              name="radioGroup"
              onChange={this.handleOptionChange}
              value="3"
            inline>
              <p>Stuart</p>
            </Radio>
              <img width={300} height={300} src="https://c.shld.net/rpx/i/s/i/spin/10065584/prod_1489158812??hei=64&wid=64&qlt=50" alt=""/>
          </FormGroup>
          <Well>Passo 2: Preencha com os dados de quem irá reservar:</Well>
          <FormGroup controlId="fullname">
            <ControlLabel>Nome Completo</ControlLabel>
            <FormControl
              onChange={this.handleChange}
              value={this.state.fullname}
              type="text"
              label="Nome Completo"
            />
          </FormGroup>
          <FormGroup controlId="email" bsClass="smallField">
            <ControlLabel>Email</ControlLabel>
            <FormControl
              onChange={this.handleChange}
              value={this.state.email}
              type="email"
            />
          </FormGroup>
          <FormGroup controlId="cpf" bsClass="smallField">
            <ControlLabel>CPF</ControlLabel>
            <FormControl
              onChange={this.handleChange}
              value={this.state.cpf}
              type="number"
            />
          </FormGroup>
          <FormGroup controlId="telephone" bsClass="smallField">
            <ControlLabel>Telefone</ControlLabel>
            <FormControl
              onChange={this.handleChange}
              value={this.state.telephone}
              type="number"
            />
          </FormGroup>
          <FormGroup controlId="address">
            <ControlLabel>Endereço</ControlLabel>
            <FormControl
              onChange={this.handleChange}
              value={this.state.address}
              type="text"
            />
          </FormGroup>
          <FormGroup controlId="cep" bsClass="smallField">
            <ControlLabel>CEP</ControlLabel>
            <FormControl
              onChange={this.handleChange}
              value={this.state.cep}
              type="number"
            />
          </FormGroup>
          <FormGroup controlId="city" bsClass="smallField">
            <ControlLabel>Cidade</ControlLabel>
            <FormControl
              onChange={this.handleChange}
              value={this.state.city}
              type="text"
            />
          </FormGroup>
          <FormGroup controlId="state" bsClass="smallField">
            <ControlLabel>Estado</ControlLabel>
            <FormControl
              onChange={this.handleChange}
              value={this.state.state}
              type="text"
            />
          </FormGroup>                                                                        
          <LoaderButton
            block
            bsStyle="primary"
            bsSize="large"
            disabled={!this.validateForm()}
            type="submit"
            isLoading={this.state.isLoading}
            text="Reservar"
            loadingText="Reservando…"
          />
        </form>
      </div>
    );
  }
}