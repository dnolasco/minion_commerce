export default {
  s3: {
    REGION: "YOUR_S3_UPLOADS_BUCKET_REGION",
    BUCKET: "YOUR_S3_UPLOADS_BUCKET_NAME"
  },
  apiGateway: {
    REGION: "us-east-1",
    URL: "https://t4hu0zsqz4.execute-api.us-east-1.amazonaws.com/prod"
  },
  cognito: {
    REGION: "us-east-2",
    USER_POOL_ID: "us-east-2_y80XxIJhQ",
    APP_CLIENT_ID: "qrs0os2bk6tql9tvkb971ih5b",
    IDENTITY_POOL_ID: "us-east-2:722189a0-ae03-4865-a730-371c8f8f82a6"
  }
};